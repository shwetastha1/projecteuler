import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Problem5 {
    /**
     * A palindromic number reads the same both ways.
     * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
     * Find the largest palindrome made from the product of two 3-digit numbers.
     */
    static void getPalindrome(){
        List<Integer> palindrome = new ArrayList<>();
        for (int i = 999; i > 99 ; i--) {
            for (int j = 999; j > 99 ; j--) {
                if (isPalindrome(i*j)) {

                    palindrome.add(i * j);
                    break;
                }
            }
        }
        Collections.sort(palindrome, Collections.<Integer>reverseOrder());
        System.out.println("palindrome = " + palindrome.get(0));
    }

    static boolean isPalindrome(int value){
        char[] numbers = String.valueOf(value).toCharArray();
        for (int i = 0; i<= numbers.length/2;i++){
            if (numbers[i]!=numbers[numbers.length-i-1])
                return false;
        }
        return true;
    }
}
