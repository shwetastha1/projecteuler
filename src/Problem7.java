public class Problem7 {
    /**
     * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
     * What is the 10 001st prime number?
     */
    static void getPrimeNumbers(){
//        String num = "600851475143";
        int limit = 10001;
        int count =0;
        int num=2;
        int prime=2;

        while(count<limit){
            boolean isPrime = true;
            for (int i = 2; i < num  ; i++) {
                if (num%i==0 && num!=i ){
                    isPrime=false;
                    break;
                }
            }
            if (isPrime){
                prime = num;
                count++;
            }
            num++;
        }
        System.out.println("prime = " + prime);




    }
}
