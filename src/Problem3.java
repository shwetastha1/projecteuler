import java.math.BigInteger;
import java.util.ArrayList;

public class Problem3 {
    /**
     * The prime factors of 13195 are 5, 7, 13 and 29.
     * What is the largest prime factor of the number 600851475143 ?
     */
    static void getPrimeFactors(){
        long number = 600851475143L;
//        long number = 13195;
        long greatestPrimeFactor = 0;
        for(long num = 2; num <= number; num++ ){
            if(number%num==0){
                if(isPrime(num)){
                    greatestPrimeFactor = num;
                }
                number/=num;
            }
        }
        System.out.println("Highest Primer Factor: " + greatestPrimeFactor);
    }

    static boolean isPrime(long num){
        if (num!=2 && num%2==0)
            return false;

        for (int i = 3; i*i <= num; i+=2) {
            if (num%i==0)
                return false;
        }
        return true;
    }
}
